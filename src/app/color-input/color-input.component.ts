import { Overlay, OverlayRef, RepositionScrollStrategy } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { Component, OnInit, Input, ElementRef, EventEmitter, Output, ComponentRef, ChangeDetectorRef } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PaletteColor } from '../color';
import { ColorInputPopupComponent } from './color-input-popup/color-input-popup.component';

@Component({
  selector: 'app-color-input',
  templateUrl: './color-input.component.html',
  styleUrls: ['./color-input.component.scss'],
  host: {
    '(document:click)' : 'checkPopupFocus()'
  }
})
export class ColorInputComponent implements OnInit {
  @Input() color : PaletteColor = new PaletteColor();
  @Output() colorChanged = new EventEmitter<PaletteColor>();
  
  private ngUnsubscribe = new Subject();
  private overlayRef : OverlayRef;
  private popupComponent ?: ComponentRef<ColorInputPopupComponent>;
  buttonOpen : boolean = false;

  constructor(private overlay : Overlay, private element : ElementRef) {
    this.overlayRef = this.overlay.create({
      hasBackdrop : false,
      positionStrategy : this.overlay.position().flexibleConnectedTo(element)
        .withPositions([{ originX: 'start', originY: 'bottom', overlayX: 'start', overlayY: 'top' }]),
      scrollStrategy : this.overlay.scrollStrategies.reposition()
    });
  }

  ngOnInit() : void {
  }

  ngOnDestroy() : void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  private doSubscribe(observable : Observable<any>, callback : (value : any) => void) {
    observable.pipe(takeUntil(this.ngUnsubscribe)).subscribe(callback);
  }

  togglePopup() {
    this.setPopupOpen(!this.popupOpen());
  }

  popupOpen() : boolean {
    return this.overlayRef.hasAttached();
  }

  setPopupOpen(open : boolean) {
    if (open) {
      if (this.overlayRef.hasAttached()) {
        this.overlayRef.detach();
      }
      this.popupComponent = this.overlayRef.attach(new ComponentPortal(ColorInputPopupComponent));
      this.popupComponent.instance.color = this.color;
      this.popupComponent.changeDetectorRef.detectChanges();

      this.doSubscribe(this.popupComponent.instance.colorChanged, color => { this.colorChanged.emit(color.clone()); })
      this.buttonOpen = true;
    }
    else {
      this.overlayRef.detach();
      this.popupComponent = undefined;
      this.buttonOpen = false;
    }
  }

  checkPopupFocus() {
    if (!this.overlayRef.overlayElement.contains(document.activeElement) &&
        !this.element.nativeElement.contains(document.activeElement)) {
      this.setPopupOpen(false);
    }
  }
}

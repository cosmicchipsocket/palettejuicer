import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { PaletteColor } from 'src/app/color';

@Component({
  selector: 'app-color-input-popup',
  templateUrl: './color-input-popup.component.html',
  styleUrls: ['./color-input-popup.component.scss']
})
export class ColorInputPopupComponent implements OnInit {
  @Input() color : PaletteColor = new PaletteColor();
  @Output() colorChanged = new EventEmitter<PaletteColor>();

  constructor() { }

  ngOnInit(): void {
  }

  emitChange() {
    this.colorChanged.emit(this.color.clone());
  }

}

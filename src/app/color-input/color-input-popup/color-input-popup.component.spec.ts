import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorInputPopupComponent } from './color-input-popup.component';

describe('ColorInputPopupComponent', () => {
  let component: ColorInputPopupComponent;
  let fixture: ComponentFixture<ColorInputPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColorInputPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorInputPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

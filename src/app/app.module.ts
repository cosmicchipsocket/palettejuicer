import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { A11yModule } from '@angular/cdk/a11y';
import { OverlayModule } from '@angular/cdk/overlay';

import { AppComponent } from './app.component';
import { ColorcelComponent } from './colorcel/colorcel.component';
import { ColorPickerComponent } from './color-picker/color-picker.component';
import { ColorInputComponent } from './color-input/color-input.component';
import { ColorInputPopupComponent } from './color-input/color-input-popup/color-input-popup.component';
import { AutoFocusDirective } from './autofocus.directive';

@NgModule({
  declarations: [
    AppComponent,
    ColorcelComponent,
    ColorPickerComponent,
    ColorInputComponent,
    ColorInputPopupComponent,
    AutoFocusDirective,
  ],
  imports: [
    A11yModule,
    BrowserModule,
    ReactiveFormsModule,
    OverlayModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

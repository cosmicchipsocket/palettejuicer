import chroma from 'chroma-js';

function clampComponent(input : number, max : number) : number {
    return Math.max(0, Math.min(input, max));
}

function wrapComponent(input : number, max : number) : number {
    return ((input % max) + max) % max;
}

export class PaletteColor {
    private _rgbR : number = 0;
    private _rgbG : number = 0;
    private _rgbB : number = 0;
    private _hslH : number = 0;
    private _hslS : number = 0;
    private _hslL : number = 0;

    clone() : PaletteColor {
        let color = new PaletteColor();

        [color._rgbR, color._rgbG, color._rgbB] = [this._rgbR, this._rgbG, this._rgbB];
        [color._hslH, color._hslS, color._hslL] = [this._hslH, this._hslS, this._hslL];

        return color;
    }

    get rgbR() { return this._rgbR; }
    get rgbG() { return this._rgbG; }
    get rgbB() { return this._rgbB; }

    set rgbR(value : number) { this._rgbR = value; this.computeFromRgb(); }
    set rgbG(value : number) { this._rgbG = value; this.computeFromRgb(); }
    set rgbB(value : number) { this._rgbB = value; this.computeFromRgb(); }

    get hslH() { return this._hslH; }
    get hslS() { return this._hslS; }
    get hslL() { return this._hslL; }

    set hslH(value : number) { this._hslH = value; this.computeFromHsl(); }
    set hslS(value : number) { this._hslS = value; this.computeFromHsl(); }
    set hslL(value : number) { this._hslL = value; this.computeFromHsl(); }

    get hex() {
        return chroma.rgb(this._rgbR, this._rgbG, this._rgbB).hex();
    }

    set hex(value : string) {
        let [r, g, b] = chroma.hex(value).rgb();

        this.setRgb(r, g, b);
    }

    setRgb(r : number, g : number, b : number) {
        this._rgbR = r;
        this._rgbG = g;
        this._rgbB = b;

        this.computeFromRgb();
    }

    static fromRgb(r : number, g : number, b : number) : PaletteColor {
        let color = new PaletteColor();

        [color._rgbR, color._rgbG, color._rgbB] = [r, g, b];
        color.computeFromRgb();

        return color;
    }

    static fromHsl(h : number, s : number, l : number) : PaletteColor {
        let color = new PaletteColor();

        [color._hslH, color._hslS, color._hslL] = [h, s, l];
        color.computeFromHsl();

        return color;
    }
    
    private computeFromRgb() {
        this.normalize();

        let [h, s, l] = chroma.rgb(this._rgbR, this._rgbG, this._rgbB).hsl();

        [this._hslH, this._hslS, this._hslL] = [isNaN(h) ? this._hslH : h, s, l];
    }

    private computeFromHsl() {
        this.normalize();

        [this._rgbR, this._rgbG, this._rgbB] = chroma.hsl(this._hslH, this._hslS, this._hslL).rgb(false);
    }

    private normalize() {
        this._rgbR = clampComponent(this._rgbR, 255);
        this._rgbG = clampComponent(this._rgbG, 255);
        this._rgbB = clampComponent(this._rgbB, 255);
        this._hslH = wrapComponent(this._hslH, 360);
        this._hslS = clampComponent(this._hslS, 1.0);
        this._hslL = clampComponent(this._hslL, 1.0);
    }
}

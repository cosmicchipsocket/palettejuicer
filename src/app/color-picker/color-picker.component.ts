import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from "rxjs/operators";
import { PaletteColor } from '../color';

@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss']
})
export class ColorPickerComponent implements OnInit {
  @Input() color : PaletteColor = new PaletteColor();
  @Input() autoFocus : boolean = false;
  @Output() colorChanged = new EventEmitter<PaletteColor>();

  private ngUnsubscribe = new Subject();

  rgbR = new FormControl(0);
  rgbG = new FormControl(0);
  rgbB = new FormControl(0);
  hslH = new FormControl(0);
  hslS = new FormControl(0);
  hslL = new FormControl(0);
  hex = new FormControl("", { updateOn : 'blur' });

  constructor() {
  }

  ngOnInit() : void {
    this.updateForm();

    this.doSubscribe(this.rgbR.valueChanges, value => { this.color.rgbR = value; this.emitChange(); });
    this.doSubscribe(this.rgbG.valueChanges, value => { this.color.rgbG = value; this.emitChange(); });
    this.doSubscribe(this.rgbB.valueChanges, value => { this.color.rgbB = value; this.emitChange(); });
    this.doSubscribe(this.hslH.valueChanges, value => { this.color.hslH = value; this.emitChange(); });
    this.doSubscribe(this.hslS.valueChanges, value => { this.color.hslS = value; this.emitChange(); });
    this.doSubscribe(this.hslL.valueChanges, value => { this.color.hslL = value; this.emitChange(); });
    this.doSubscribe(this.hex.valueChanges, value => {
      try {
        this.color.hex = value;
        this.emitChange();
      }
      catch {}
    });
  }

  ngOnDestroy() : void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  roundNumber(value : number) : number {
    return Math.round(value * 1000) / 1000;
  }

  private doSubscribe(observable : Observable<any>, callback : (value : any) => void) {
    observable.pipe(takeUntil(this.ngUnsubscribe)).subscribe(callback);
  }

  private emitChange() {
    this.colorChanged.emit(this.color.clone());
  }

  private updateForm() {
    this.rgbR.setValue(this.roundNumber(this.color.rgbR), {emitEvent : false});
    this.rgbG.setValue(this.roundNumber(this.color.rgbG), {emitEvent : false});
    this.rgbB.setValue(this.roundNumber(this.color.rgbB), {emitEvent : false});

    this.hslH.setValue(this.roundNumber(this.color.hslH), {emitEvent : false});
    this.hslS.setValue(this.roundNumber(this.color.hslS), {emitEvent : false});
    this.hslL.setValue(this.roundNumber(this.color.hslL), {emitEvent : false});

    this.hex.setValue(this.color.hex, {emitEvent : false});
  }

}

import { PaletteColor } from "./color";

const TEST_COLORS : [[number, number, number], [number, number, number]][] = [
    [[0, 0, 0], [0, 0, 0]],
    [[255, 255, 255], [0, 0, 1.0]],
    [[255, 0, 0], [0, 1.0, 0.5]],
    [[0, 255, 0], [120, 1.0, 0.5]],
    [[0, 0, 255], [240, 1.0, 0.5]],
    [[255, 255, 0], [60, 1.0, 0.5]],
    [[0, 255, 255], [180, 1.0, 0.5]],
    [[255, 0, 255], [300, 1.0, 0.5]],
    [[191, 191, 191], [0, 0, 0.75]],
    [[128, 128, 128], [0, 0, 0.5]],
    [[128, 0, 0], [0, 1.0, 0.25]],
    [[127, 128, 0], [60, 1.0, 0.25]],
    [[0, 128, 0], [120, 1.0, 0.25]],
    [[0, 127, 128], [180, 1.0, 0.25]],
    [[0, 0, 128], [240, 1.0, 0.25]],
    [[128, 0, 127], [300, 1.0, 0.25]],
    [[191, 64, 64], [0, 0.5, 0.5]],
    [[191, 128, 64], [30, 0.5, 0.5]],
    [[191, 191, 64], [60, 0.5, 0.5]],
    [[64, 191, 64], [120, 0.5, 0.5]],
    [[64, 191, 191], [180, 0.5, 0.5]],
    [[64, 64, 191], [240, 0.5, 0.5]],
    [[191, 64, 191], [300, 0.5, 0.5]],
];

function compareRgb(actual : PaletteColor, r : number, g : number, b : number) {
    expect(actual.rgbR).toBeCloseTo(r, 0);
    expect(actual.rgbG).toBeCloseTo(g, 0);
    expect(actual.rgbB).toBeCloseTo(b, 0);
}

function compareHsl(actual : PaletteColor, h : number, s : number, l : number) {
    expect(actual.hslH).toBeCloseTo(h, 0);
    expect(actual.hslS).toBeCloseTo(s, 2);
    expect(actual.hslL).toBeCloseTo(l, 2);
}

describe("PaletteColor", () => {
    it("constructs an rgb color", () => {
        let color = PaletteColor.fromRgb(20, 40, 60);

        compareRgb(color, 20, 40, 60);
    });

    it("constructs an hsl color", () => {
        let color = PaletteColor.fromHsl(200, 0.75, 1.0);

        compareHsl(color, 200, 0.75, 1.0);
    });

    it("converts rgb to hsl correctly", () => {
        for (const [rgb, hsl] of TEST_COLORS) {
            let newHsl = PaletteColor.fromRgb(rgb[0], rgb[1], rgb[2]);

            compareHsl(newHsl, hsl[0], hsl[1], hsl[2]);
        }
    });

    it("converts hsl to rgb correctly", () => {
        for (const [rgb, hsl] of TEST_COLORS) {
            let newRgb = PaletteColor.fromHsl(hsl[0], hsl[1], hsl[2]);

            compareRgb(newRgb, rgb[0], rgb[1], rgb[2]);
        }
    });
});

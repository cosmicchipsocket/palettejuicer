import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appAutoFocus]'
})
export class AutoFocusDirective {
  private focus = false;

  constructor(private element : ElementRef) {}

  ngOnInit() {
    this.updateFocus();
  }

  private updateFocus() {
    if (this.focus) { 
      window.setTimeout(() => {
        this.element.nativeElement.focus();
      });
    }
  }

  @Input() set appAutoFocus(enabled : boolean) {
    this.focus = enabled;
    this.updateFocus();
  }
}

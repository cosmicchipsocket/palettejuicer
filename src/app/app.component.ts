import { Component, ViewEncapsulation } from '@angular/core';
import { PaletteColor } from './color';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'palettejuicer';
  color : PaletteColor = PaletteColor.fromHsl(330, 0.85, 0.625);
  inverted : PaletteColor = PaletteColor.fromRgb(0, 0, 0);
  color3 : PaletteColor = PaletteColor.fromHsl(130, 0.85, 0.625);

  testComputeColor(color : PaletteColor) {
    this.color3 = color.clone();
    this.inverted = PaletteColor.fromRgb(255 - color.rgbR, 255 - color.rgbG, 255 - color.rgbB);
  }
}

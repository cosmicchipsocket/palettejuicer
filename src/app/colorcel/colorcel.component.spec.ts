import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorcelComponent } from './colorcel.component';

describe('ColorcelComponent', () => {
  let component: ColorcelComponent;
  let fixture: ComponentFixture<ColorcelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColorcelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorcelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

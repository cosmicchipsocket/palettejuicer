import { Component, OnInit, Input } from '@angular/core';
import { PaletteColor } from '../color';

@Component({
  selector: 'app-colorcel',
  templateUrl: './colorcel.component.html',
  styleUrls: ['./colorcel.component.scss']
})

export class ColorcelComponent implements OnInit {
  @Input() color : PaletteColor;

  constructor() {
    this.color = new PaletteColor();
  }

  ngOnInit(): void {
  }

}
